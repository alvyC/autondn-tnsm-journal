\subsection{Trust Model}
\label{sec:trust}

As explained in Section~\ref{sec:threats}, in order to reduce the likelihood of accepting false information, a vehicle should authenticate all received data.  Since every data packet in NDN is signed by the producer, it can be verified using the data's signature and a pre-defined trust model for the data (Section~\ref{sec:background}).  Every entity in NDN has its corresponding key(s) that can be used to sign certain data and/or keys.  The trust model defines the trust anchor(s) and the chain of trust from the anchor(s) to the data, i.e., the key/data signing privileges of each entity in the chain.

To ensure data authenticity in vehicular networks, we propose a four-level trust model including automotive organizations, manufacturers, vehicles and data (Table~\ref{table:kname}).   First, the trust anchors can be highly reputable organizations that are interested in the deployment of vehicular networking technology.  They are responsible for certifying the keys belonging to the automotive manufacturers by signing those keys.  There can be \emph{multiple} trust anchors and a manufacturer can obtain a certificate from one or more of them (each vehicle can be configured with multiple trust anchors' public keys).    Second, every manufacturer certifies the keys of its vehicles.   Third, every vehicle signs its own data.

The certification of a key involves checking the credentials of the entity being certified.  For example, a manufacturer needs to present valid documents proving that it is indeed the claimed manufacturer.  A vehicle can present a unique secret vehicle identifier (VID) that the manufacturer installed in the vehicle in the factory (see Section~\ref{sec:privacy}).

Based on the above trust model, a vehicle's data and signature can be verified by the vehicle's key, which can be verified by the manufacturer's key, and the manufacturer's key can be verified by a trust anchor's key, which is self-signed and pre-configured in the application.  Figure~\ref{fig:trustmodel} shows an example of the signing and verification process, with the organization AUVSI (Association for Unmanned Vehicle Systems International) being the trust anchor.

The trust schema based on our trust model should include the following trust rules: (1) the vehicle key that signs a piece of vehicular data should have the \textit{\textless vehicle-name\textgreater} component of the data name as its prefix; (2) the manufacturer's key signing a vehicle's key should match in their first component; (3)  the key that signs a manufacturer's key should match one of the trust anchors configured locally.  Section~\ref{sec:comm-module} presents an implementation of this schema for our AutoNDN application. Listing~\ref{list:trustschema} is an example of the trust schema configured in our AutoNDN application.

 \lstset{
           basicstyle=\ttfamily\scriptsize,
           numbers=left,
           numberstyle=\tiny,
           numbersep=1pt,
           breaklines=true
          }

\begin{lstlisting}[float=*, firstnumber=1, multicols=2, caption = {Schema for Data Validation}, label=list:trustschema]

security
{
  validator
  {
    rule
    {
      id "Road Status Rule"
      for data
      filter
      {
        type name
        regex ^<autondn><road-status><>+
      }
      checker
      {
        type customized
        sig-type rsa-sha256
        key-locator
        {
          type name
          hyper-relation
          { ; <make-id><vehicle-id><KEY><ksk-123><ID-CERT>
            k-regex ^([^<KEY>]*)<KEY><ksk-.*><ID-CERT>$
            k-expand \\1 ; extract vehicle's name
            h-relation equal
            ; /autondn/road-status/<data-location>/<time-stamp>/V1.Vehicle/<make-id><vehicle-id>
            p-regex ^<autondn><road-status><>+<V1.Vehicle>(<><>)$
            p-expand \\1 ; extract vehicle name
          }
        }
      }
    }
    rule
    {
      id "Hierarchical Rule"
      for data
      filter
      {
        type name
        regex <><><KEY><ksk-.*><ID-CERT><>$
      }
      checker
      {
        type hierarchical
        sig-type rsa-sha256
      }
    }

  rule
  {
     id "Hierarchical Exception Rule"
     for data
     filter
     {
      type name
      regex <><KEY><ksk-.*><ID-CERT><>$
     }
     checker
     {
      type fixed-signer
      sig-type rsa-sha256
      signer
      {
    	type file
    	file-name "autondn-root.cert"
      }
     }
   }
   trust-anchor
   {
    type file
    file-name "autondn-root.cert"
   }
 }
; validator

  cert-to-publish "honda.cert"
  cert-to-publish "honda-vehicle.cert"
}
\end{lstlisting}

We can extend our trust model to accommodate other entities in the system.  For example, due to privacy considerations, we will introduce Certificate Issuing Proxies (CIP) to help vehicles obtain manufacturers' pseudonyms and the certificates of their public keys from the manufacturers.   In order for a vehicle to trust a proxy, the proxy's key must be certified by the vehicle's manufacturer.  However, the proxy does not have the authority to certify vehicles' keys, because it cannot verify the identities of the vehicles.

\begin{figure}
 \centering
  \includegraphics[width=8.3cm]{img/trust-hierarchy.jpg}
  \caption{Example Signing and Verification Hierarchy (Real names such as Honda are used here for illustration purposes only.  Due to privacy considerations, we will use pseudonyms to replace the real names as described in Section~\ref{sec:privacy}.)}
  \label{fig:trustmodel}
\end{figure}

\subsection{Vehicle Tracking Prevention}
\label{sec:privacy}

%The trust model depends upon the naming scheme and as such any change in naming can break the verification.
Our trust model requires a vehicle to put its name into its data's name and key name.  This gives rise to privacy concerns as the vehicle can be easily tracked if it keeps using the same name to publish its data.  To mitigate the threat of vehicle tracking, rather than changing the trust model, we propose to \emph{let each vehicle use a set of pseudonyms so that it can publish consecutive data items using different pseudonyms and the associated keys}.  Each vehicle's complete pseudonym $n$ has two components /$<$MP$>$/$<$VIP$>$, where $<$MP$>$ is the manufacturer's pseudonym and $<$VIP$>$ is the vehicle's individual pseudonym.  MPs are generated by the manufacturers and provided to the vehicles.  VIPs are generated by vehicles whenever they need new pseudonyms.  We use pseudonyms for manufacturers to prevent leaking information about vehicles.  Otherwise, if a vehicle travels along a road segment with few other vehicles, it will produce a series of data items containing the same manufacturer name, the same road segment and similar timestamps.  Such information can be used to infer that the data items belong to the same vehicle.  

Since a vehicle cannot have an unlimited number of pseudonyms, it will have to reuse a pseudonym after time $T$, which is determined by the number of pseudonyms $N$ and the average data-publishing rate $R$, i.e., $T = N/R$.  The longer the $T$, the more difficult it is to track the vehicle.  This means that, for a given date-publishing rate, a larger $N$ is more desirable.  We envision that every vehicle will have a large number of pseudonyms and certificates initially installed by its manufacturer.  However, as the keys expire over time, every vehicle needs to generate new pseudonyms and the associated keys in order to maintain a sufficient number of pseudonyms.
% TODO: talk about how unique pseudonyms can be generated using seed 
To bootstrap this process, each vehicle is given a secret vehicle identifier (VID) by its manufacturer.  The VID is used as an input to generate pseudonyms so as to prevent pseudonym collision among vehicles.  It is also used by vehicles to prove their identity when they request for certificates.

The certificate issuing process can potentially expose part of a vehicle's identity information.  More specifically, if a vehicle requests for a new certificate by sending an interest containing the manufacturer's name prefix, an attacker who captures packet traces in the vehicular network can infer that a vehicle from that manufacturer was near a certain location at a particular time.  Hence we need to provide a \emph{certificate issuing mechanism that is resistant to vehicle tracking}.  To this end, we introduce a new entity to the system called \emph{\textbf{Certificate Issuing Proxy (CIP)}} which requests certificates on a vehicle's behalf.  A CIP may be located at a gas station, car dealer, road-side unit, or even home (Figure~\ref{fig:cert-issuance}). The idea is similar to periodically filling up gas in a vehicle, except that in this case we fill up certificates.  To request for a new certificate via a CIP, the vehicle sends an interest using the CIP's name prefix, and the CIP relays the interest to the manufacturer using a network that is only accessible by authorized personnel at the facility (not an open-access wireless link) so that it is difficult for an attacker to capture a packet trace directly from the CIP.   
%This way vehicles will have a sufficient number of pseudonyms and certificates at a given time.

\begin{figure}
\includegraphics[width=9cm]{img/mechanics.png}
\caption{Obtaining Certificates from Manufacturer and Proxies}
\label{fig:cert-issuance}
\end{figure}

Each CIP needs to obtain its own certificates from manufacturers, so that vehicles and manufacturers can trust it.  Before a vehicle uses a CIP's service, it requests for all the CIP's certificates and verifies that at least one of the certificates is from its manufacturer.  Some CIPs may have very limited capabilities.  For example, a home CIP may be only authorized to relay requests for specific vehicles owned by the homeowners.  These rights may be specified using certain name components in the CIP's key.

\begin{figure*}[t]
	\centering
	\includegraphics[width=5.5in]{img/id-renewal-new.png}
	\caption{Pseudonym Certificate Issuance through Proxy}
	\label{fig:privacy}
\end{figure*}

\iffalse
\begin{figure}
\centering
\includegraphics[width=4in]{img/id_renewal.png}
\caption{Pseudonym Certificate Issuance through Proxy}
\label{fig:privacy}
\end{figure}
\fi
Note that when a vehicle sends an interest for a new certificate to a CIP, it needs to include the actual manufacturer's name, encrypted using the CIP's key, as one of the name components. This is for the CIP to route the interest to the correct manufacturer.  Below we explain the scheme for certificate issuance  as shown in Figure~\ref{fig:privacy}

\begin{enumerate}
  \item The vehicle first retrieves the CIP's public key $K_1$ and associated certificates, which are issued by various manufacturers. The vehicle then validates that one of the certificates is from its manufacturer.
  % AG: Figure says K3 instead of K2
  \item The vehicle generates a random pseudonym based on its VID, chooses a manufacturer's pseudonym from those received from the manufacturer, and creates a new key $K_2$. It encrypts its VID and the new public key $K_2$ using the manufacturer's public key ${K_0}$. It then encrypts this information along with the manufacturer's name using the CIP's key $K_1$. Next, it sends an interest ($I_1$) to the CIP with the name \textit{/\textless CIP\textgreater/$E_{K_1}$($<$manufacturer$>$, $E_{K_0}$(VID, $K_2$))}.
  \item CIP receives $I_1$, decrypts the component that follows \textit{\textless CIP\textgreater} and determines the manufacturer's name. CIP cannot decrypt the components after manufacturer's name as they are encrypted using ${K_0}$. This ensures that CIP cannot obtain the VID and other sensitive information.  Next, CIP constructs and sends a new interest ($I_2$), signed with ${K_1}$, with the name \textit{/\textless manufacturer\textgreater/$E_{K_0}$(VID, $K_2$)}.
  \item After receiving $I_2$, the manufacturer checks that the interest is signed by one of the authorized proxies and then extracts the VID and $K_2$. The manufacturer verifies the VID and creates a certificate for the new key $K_2$.  It then encrypts this certificate, a future manufacturer's pseudonym, and the manufacturer's pseudonym certificate, using $K_2$, to prevent the CIP from obtaining the information.\footnote{The vehicle can publish the manufacturer's pseudonym certificate when it publishes new data with a name containing that manufacturer's pseudonym to speed up validation of the data by other vehicles.} This information is included as content in the data packet $D_1$.
  \item CIP receives the data packet $D_1$, constructs $D_2$ by changing its name to match $I_1$, and forwards it to the vehicle, which will be able to decrypt the data and store the certificate and other information. The manufacturer's future pseudonym and certificate will be used by the vehicle the next time it creates a pseudonym.
\end{enumerate}


If we use a unique manufacturer's pseudonym for every vehicle's pseudonym, then there will be too many such pseudonyms and keys for the manufacturer to generate and get certified.  This is not a scalable solution.  Instead, the manufacturer can give the same manufacturer pseudonym and key to different vehicles in different geographic areas, e.g., one pseudonym may be used by 50 vehicles each in a different country.  This kind of reuse will lower the risk of vehicle tracking through the manufacturer's pseudonym.
