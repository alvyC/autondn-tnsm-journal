\section{A Generic Trust Model for Vehicular Applications}
\label {sec:design}
In this section we first describe some typical vehicular applications and present a naming scheme supporting these applications.  We then propose a generic trust model for authenticating information from vehicles participating in these applications.
% In this trust model we do not consider location-awareness or time-sensitivity. 
We further develop the bootstrapping procedure for the model and propose a preliminary framework for protecting the privacy of the vehicles (and their owners) when the vehicles share data that may disclose their location information.
%Note that this work does not address efficient routing/forwarding of interests and data.

\subsection{Vehicular Networking Applications}

Each vehicle has a set of sensors that capture information about the vehicle and its environment.  Data collected by the vehicles can be of various types, such as its own direction and speed, other vehicles' direction and speed, traffic signs, potholes, and accidents.   Safety applications can utilize speed and direction data to prevent collision. Autonomous driving software can use a vehicle's own sensor data, other vehicles' data, maps, and other data to make all the driving decisions for a vehicle.
In addition, infotainment applications can stream movies, music, news, and sports from remote sources. 
%It should be noted that some of the applications are time-sensitive and location-dependent, which should be taken into account while designing a security protocol for this environment.

\begin{figure}
	\includegraphics[width=9cm]{img/overview.png}
	\caption{A Scenario of Data Sharing among Vehicles}
	\label{fig:data-sharing}
\end{figure}

A simple example of a vehicular application is illustrated in Figure~\ref{fig:data-sharing}. In this example, a pedestrian is trying to cross the road and at the same time Car A needs to turn right. Car A cannot sense the pedestrian as its view is blocked by the tree. However, Car B can sense the person and it is within the transmission range of Car A. Therefore, Car B can provide this information to Car A and prevent an accident.

%\footnote{Interested readers can refer to work by Grassi et. al. (\cite{grassi2014vanet,grassi2015navigo}) to learn more about interest/data forwarding issues in NDN-based vehicular networks.} 
In an NDN-based vehicular network, vehicles send NDN interests via V2V and V2I communication to retrieve data that may potentially affect their decisions.
% such as detected pedestrians and cyclists in the surrounding area, traffic level of the next road segment it will travel on, and accidents on its path to the destination.
Any vehicle that has relevant information to share may respond to an interest. To avoid an implosion of data responses, a vehicle receiving an interest may use a random back-off scheme before sending its data -- it will suppress its own reply if it hears another reply during the back-off period. Finally, the data may be cached by any vehicle that overhears it and the cached data can satisfy any interest it matches as the vehicle moves around. %\cite{grassi2014vanet,grassi2015navigo, chowdhury2020leveraging} are some of the proposed interest forwarding strategies for a vehicular network.

\subsection{Naming Scheme}
\label{sec:naming}

Naming is one of the most important aspects in designing an NDN application~\cite{ndn-ccr2014}. It is closely related to other parts of the application design, e.g., the trust model for authenticating vehicular data, and it also affects how efficiently the packets from the application can be routed and forwarded.  Based on the expected participants and data in the vehicular applications, we designed the following naming scheme for them.

Our data name follows this format: \textit{\small /\textless application-prefix\textgreater/\textless data-type\textgreater/\textless data-location\textgreater/\textless name-marker\textgreater/\textless vehicle-name\textgreater/\textless timestamp\textgreater}.  Similar to the naming scheme proposed by Wang et. al. in~\cite{dnv2v}, our name format contains geographical scoping and temporal scoping information.  However, a major difference from \cite{dnv2v} is that our scheme contains the \emph{name (Section~\ref{sec:trust}) or pseudonym (Section~\ref{sec:privacy}) of the vehicle that produced the data} so that we can associate the data with the origin vehicle and authenticate the data.  In addition, because we have vehicle name and timestamp in the data name, we do not need a nonce field to make the names unique as in~\cite{dnv2v}.  Below we explain each name component in more detail.

The first component is the \emph{name prefix of the application}, such as a real-time traffic application or an accident prevention application, which is producing the data.

The \emph{data type} component refers to the kind of data that is produced, e.g., status of the road, sudden speed reduction of other vehicles, or arrival of emergency vehicles. Different types of data require vehicles to process them differently and take different actions, so it is important to differentiate between them.

Since vehicular data is mostly location dependent, we use a name component \textit{data location} to specify the geolocation of the data.   Depending on the application, the location name may have different formats -- one option is a hierarchical structure that allows vehicles to get information of a road in different granularity, e.g., \textit{/usa/tn/memphis/road-name/section1-section2} and another option is a pair of longitudinal and latitudinal coordinates.

The next component \textit{name marker} indicates that the following name component is the vehicle's name.  We need this marker because the location may contain multiple name components so it is difficult to know where the vehicle name starts. In our implementation, we use \emph{V1.Vehicle} as the marker.  

The \emph{vehicle name} should reflect a vehicle's relationships with other entities in the system and allow derivation of trust based on its relationships.  In our design, it has a simple form \textit{/$<$manufacturer-name$>$/$<$vehicle-name$>$}, where the first component identifies the vehicle's manufacturer and second component identifies the vehicle.  These components are necessary for data authentication as we will explain in Section~\ref{sec:trust}.  Note that it is possible to track the vehicle when  the vehicle is associated with a single name (Section~\ref{sec:threats}).  To address this problem, we propose to use pseudonyms to make vehicle tracking difficult (Section~\ref{sec:privacy}).

The next component is \textit{timestamp}, which can either be a single value, i.e., a particular point in time, or a range.

\begin{table*}
\centering
	\caption{Trust Hierarchy, Key Names and Data Names in Vehicular Applications}
	\begin{tabular}{|p{0.2in}|p{0.7in}|p{2.8in}|p{2.3in}|}
	\hline
	\bfseries {Level} & \bfseries {Entity} & \bfseries {Key/Data Name} & Example\\ \hline
	1 & Automotive Organization & /$<$org$>$/KEY/$<$key-id$>$ & /auvsi/KEY/$<$key-id$>$\\ \hline
	2 & Manufacturer & /$<$manufacturer-name$>$/KEY/$<$key-id$>$ & /honda/KEY/$<$key-id$>$\\ \hline
	3 & Vehicle & /$<$manufacturer-name$>$/$<$vehicle-name$>$/KEY/$<$key-id$>$ & /honda/574G98627Y/KEY/$<$key-id$>$\\\hline
	4 & Data & /$<$application-prefix$>$/$<$data-type$>$/$<$data-location$>$/$<$name-marker$>$/$<$vehicle-name$>$/$<$timestamp$>$ & /autondn/road-status/usa/tn/memphis/I-240/20-21//honda/574G98627Y/20170128105534 \\\hline
	\end{tabular}
	\label{table:kname}
	%\vspace{-3mm}
\end{table*}

Our AutoNDN prototype system follows the above naming scheme, and an example data name is \url{/autondn/road-status/usa/tn/memphis/I-240/20-21/V1.Vehicle/<vehicle-name>/20210128105534}, which indicates that the data is about the status of the interstate I240 between exit 20 and exit 21 at 10:55:34 on Jan. 28, 2021 (see Section~\ref{sec:system} for more information about our prototype).

We note that our data naming scheme may not fit every vehicular application.  Some applications may need additional name components, while others may not need all the name components or may prefer different ordering of the name components.  In NDN, each application defines its own naming scheme and trust model.  We simply provide an example naming scheme that may work for typical vehicular applications. 
% If a name component is not expected to be useful in interests (as a query parameter), yet it still provides useful information to consumers, it may be moved into the data content.
