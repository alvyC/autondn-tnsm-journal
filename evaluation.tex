\section{Evaluation}
\label{sec:evaluation}
\subsection{Trust Establishment Scheme}
We evaluate our trust establishment scheme both qualitatively and quantitatively.  For \textbf{qualitative analysis}, we examine a few desirable properties of trust management schemes in a vehicular ad-hoc network (VANET), as proposed by Zhang~\cite{zhang2011survey}.  
%we discuss these properties and investigate how many of them are available in our trust system.
First and foremost, a VANET trust management scheme should be \emph{decentralized}, i.e., vehicles should not need to contact a third-party to establish trust relationship during their communication, as contacting a third-party will delay the consumption of data which can affect time critical applications.  Our trust models are decentralized in the sense that when a vehicle needs a certificate to authenticate a Data packet, it does not have to rely on any central entity. If the data is produced and served by another vehicle, then that vehicle can also serve the necessary certificates. Data can also be bundled with certificates so that even if the data is served by a vehicle that cached it, that vehicle should also have the required certificates in its cache. 

Second, the scheme should work even when the vehicles are \emph{sparse}.  As we explained above, in our schemes, a vehicle receiving a Data packet can retrieve the certificates from the vehicle serving the data, so the data authentication does not depend on the number of vehicles in the network.
%, certificate retrieval works in a decentralized way that makes the system sparsity tolerant and scalable. 

Third, the scheme should be \emph{scalable}, e.g., a message for trust verification should not require all the vehicles to respond.  Our schemes are scalable in that each vehicle needs to retrieve only a few certificates for each namespace as specified in the trust model and these certificates can be saved for subsequent verification of data in the same namespace (i.e., no need to fetch them again). 

Fourth, the scheme should provide \emph{system level security}, which means allowing peers to authenticate themselves, i.e., prove their identity.  This is exactly what our schemes provide.
%\cite{zhang2011survey} also argues that trust should event/task and location/time specific.

Fifth, the scheme should be \emph{sensitive to privacy concerns}.  Our scheme allows vehicles to prove their identity using constantly changing pseudonyms, thus addressing the concern of vehicle tracking. 

\begin{comment}
\begin{table}
  \centering
  \caption{Desired Characteristics of VANET Trust Model}
   \begin{tabular}{|c | c |}
   \hline
   \textbf{Characteristic} & \textbf{Status} \\ [0.5ex]
   \hline\hline
   Decentralization & Present \\
   \hline
   Sparsity Tolerance & Present \\
   \hline
   Event/Task and Location/Time Specificity & Absent \\
   \hline
   Scalability & Present \\
   \hline
	 Measure of Confidence &  Absent\\
	 \hline
	 System Level Security & Present \\
	 \hline
	 Privacy & Present \\ 
   \hline
	\end{tabular}
\label{table:vanet-char}
\end{table}
\end{comment}

%Table~\ref{table:vanet-char} summarizes which of the characteristics are present in our trust model. 
Our proposed schemes do not have the following properties.  First, our schemes do not 
capture the uncertainty of trust using a \emph{confidence measure}.  Second, while our geolocation based trust model establishes trust based on location, we do not propose trust models that consider event, task, or time. These are our future research areas.

For \textbf{quantitative analysis}, we have conducted experiments that consist of seven nodes: one root organization, three manufacturers, and three mini cars. The root organization issues certificate for the manufacturers, which in turn issue certificates for their respective manufactured cars. Besides serving its own certificate, each car also serves the certificate of its manufacturer to speed up data validation by other cars.  The certificate of the trust anchor is configured in each car.
The cars are equipped with Raspberry Pi of three different models, namely Raspberry Pi Model B (RPi-B), Raspberry Pi-2 Model B (RPi-2B) and Raspberry Pi-3 Model B(RPi-3B). Table~\ref{table:rpis} shows the specification of the RPi's. The CPU power increases significantly from RPi-B to RPi-2B, but the difference between RPi-2B and RPi-3B is relatively small.   In addition, the two later models have twice the RAM as the first model.  Note that we installed the same OS and kernel version on all three RPi's.

\begin{table}
  \centering
  \caption{Specification of On-board RPi's}
   \begin{tabular}{|c | c | c | c|}
   \hline
   SoC & RPi-B & RPi-2B & RPi-3B \\ [0.5ex]
   \hline\hline
   Core Type & ARM1176JZF-S & Cortex-A7 & Cortex-A53 64-bit \\
   \hline
   No. Of Cores & 1 & 4 & 4 \\
   \hline
   CPU Clock & 700 MHz & 900 MHz & 1.2 GHz \\
   \hline
   RAM & 512 MB & 1 GB & 1 GB \\
   \hline
  \end{tabular}
\label{table:rpis}
\end{table}

% rpi1_con_rpi2_prod.pdf
% rpi2_con_rpi1_prod.pdf
% rpi1_con_rpi3_prod.pdf
% rpi3_con_rpi1_prod.pdf
% rpi2_con_rpi3_prod.pdf
% rpi3_con_rpi2_prod.pdf


\begin{figure}
    \centering
  \subfloat[Consumer: RPi1, Producer: RPi2\label{1a}]{%
       \includegraphics[width=0.5\linewidth]{plot/rpi1-con-rpi2-prod.pdf}}
    \hfill
  \subfloat[Consumer: RPi2, Producer: RPi1\label{1b}]{%
        \includegraphics[width=0.5\linewidth] {plot/rpi2-con-rpi1-prod.pdf}}
    \\
  \subfloat[Consumer: RPi1, Producer: RPi3\label{1c}]{%
        \includegraphics[width=0.5\linewidth] {plot/rpi1-con-rpi3-prod.pdf}}
    \hfill
  \subfloat[Consumer: RPi3, Producer: RPi1\label{1d}]{%
        \includegraphics[width=0.5\linewidth] {plot/rpi3-con-rpi1-prod.pdf}}
  \\
	\subfloat[Consumer: RPi2, Producer: RPi3\label{1e}]{%
        \includegraphics[width=0.5\linewidth] {plot/rpi2-con-rpi3-prod.pdf}}
    \hfill
  \subfloat[Consumer: RPi3, Producer: RPi2\label{1f}]{%
        \includegraphics[width=0.5\linewidth] {plot/rpi3-con-rpi2-prod.pdf}}

	\caption{Consumer and Producer Delays in Different Runs
		(\textbf{Consumer Delay}: Data Fetching Delay (DFD), Certificate Fetching Delay (CFD),
		Data Validation Delay (DVD);  \textbf{Producer Delay}: Data Signing Delay (DSD), Certificate Sending Delay (CSD))
		}

	\label{fig:delays}
\end{figure}

\iffalse
\begin{figure*}[t!]
    \centering
    
		\begin{subfigure}[t]{.5\textwidth}
			\centering
			\includegraphics[height=1.5in] {plot/rpi1-con-rpi2-prod.pdf}
			\caption{Consumer: Raspberry Pi1, Producer: Raspberry Pi2} 
		\end{subfigure}%
		\begin{subfigure}[t]{.5\textwidth}
			\centering
			\includegraphics[height=1.5in] {plot/rpi2-con-rpi1-prod.pdf}
			\caption{Consumer: Raspberry Pi2, Producer: Raspberry Pi1}
		\end{subfigure}
    
		\begin{subfigure}[t]{.5\textwidth}
			\centering
			\includegraphics[height=1.5in] {plot/rpi1-con-rpi3-prod.pdf}
			\caption{Consumer: Raspberry Pi1, Producer: Raspberry Pi3}
		\end{subfigure}%
		\begin{subfigure}[t]{.5\textwidth}
			\centering
			\includegraphics[height=1.5in] {plot/rpi3-con-rpi1-prod.pdf}
			\caption{Consumer: Raspberry Pi3, Producer: Raspberry Pi1}
		\end{subfigure}
    
		\begin{subfigure}[t]{.5\textwidth}
			\centering
			\includegraphics[height=1.5in] {plot/rpi2-con-rpi3-prod.pdf}
			\caption{Consumer: Raspberry Pi2, Producer: Raspberry Pi3}
		\end{subfigure}%
		\begin{subfigure}[t]{.5\textwidth}
			\centering
			\includegraphics[height=1.5in] {plot/rpi3-con-rpi2-prod.pdf}
			\caption{Consumer: Raspberry Pi3, Producer: Raspberry Pi2}
		\end{subfigure}
    
		\caption{Consumer and Producer Delays in Different Runs
		(\textbf{Consumer Delay}: Data Fetching Delay (DFD), Certificate Fetching Delay (CFD),
		Data Validation Delay (DVD);  \textbf{Producer Delay}: Data Signing Delay (DSD), Certificate Sending Delay (CSD))
		}

		\label{fig:delays}

\end{figure*}
\fi

We are interested in the total delay between the time when a car sends an interest and the time when it validates the received data.  This delay depends on whether the certificates needed to validate the data are locally available or not. If the certificates are not available then the cars need to fetch two certificates, the data producer's and the manufacturer's. Thus, we partitioned the total delay into various delay components to examine their contribution.  Consumer-side delay includes data fetching delay, certificate fetching delay, and validation delay.  Producer-side delay consists of signing delay and certificate sending delay. 

In each of our experiments, two cars run, one as a consumer and another as a producer, ten times, and the averages of the corresponding delays are taken.
The producer car travels on the experiment track first and generates data for the roads, and the consumer car follows the producer and gets data from it. The number of interest-data exchange in each run is the same as the number of roads in the track (ten). After the first time a car (${car_1}$) consumes data from another car (${car_2}$), the former has all the certificates needed to authenticate data from the latter. Consequently in the subsequent data consumption from ${car_2}$, ${car_1}$ does not have to request for the necessary certificates, i.e., there is no certificate fetching delay.  Therefore, we only considered the first interest-data exchange in each run when calculating the certificate fetching delay. To erase a car's memory of its interactions with another car, NFD is restarted in both cars before each run.
 
Figure~\ref{fig:delays} shows our experiment results.  The data fetching delay of the consumer includes the delay for the consumer's interest to reach the producer, the data signing delay of the producer (RSA is used for data signing), and the delay for the data to reach the consumer. The lower the processing power of the producer, the higher the data signing delay, and the higher the fetching delay from the producer.  Due to RPi1's much lower CPU power, its data signing delay is more than twice that of the other two RPi's, so retrieving data from RPi1 is much slower.  The certificate sending delay of the producer mainly includes the time to calculate a SHA256 digest of a certificate.   Because the digest calculation is much faster than RSA signing, the certificate sending delay is much smaller than the data signing delay.  Data validation delay is how much time it takes for a car to validate data after retrieving certificates.  As such, it depends on the processing power of the car so the RPi1 takes twice as much time to validate the data compared to the other two RPi models.  However, because the RSA algorithm has faster validation than signing, the data validation delay is significantly smaller than the data signing delay.

The good performance demonstrated by RPi2 and RPi3, as shown in Figure~\ref{fig:delays}(c) and (d), is encouraging.  Since real vehicles can have even more powerful CPUs and higher storage capacity, the delays caused by these factors will be even smaller for real vehicles.

\subsection{Pseudonym Renewal Scheme}
\textbf{Formal Analysis} We analyze the protocol using CPSA~\cite{doghmi2007searching} assuming that the vehicle already has the manufacturer's public key and the CIP's public key, and it trusts both of them. We check, from the vehicle's point of view, whether the manufacturer's name is leaked. Then we check whether the issued certificate is leaked from the CIP or the manufacturer. The realized cryptographic shapes are found to be secure as they fail to show any disagreements on the sent and received values as well as any leaking of the manufacturer's name or certificate.

In the fully realized execution, the vehicle is not shown by CPSA since anybody can initiate the request. However, the manufacturer will verify the VID sent by the vehicle and only then issue the certificate. If the attacker tries to replay the vehicle's Interest, the manufacturer will not issue another certificate for the same vehicle key.
% AG: Should we include our scheme specification in the appendix and mention it here?

\textbf{Performance Analysis} To provide insight into the communication cost, we simulated the pseudonym renewal scheme in an ns-3~\cite{henderson2008network} based open source NDN network simulator called ndnSIM~\cite{ndnSIM2}. The simulation scenario consists of three nodes each assuming the role of vehicle, CIP, and manufacturer, respectively. The Vehicle and CIP are communicating over a wireless channel (the specific settings are detailed in Table~\ref{tab:vehcip}). On the other hand, the CIP and manufacturer are connected through a wired link.
\begin{figure*}[t]
	\centering
	\includegraphics[width=6in, height=1.2in]{img/id-renewal-delay.png}
	\caption{Delay of Pseudonym Certificate Issuance through Proxy: S, R, and P stands for Send, Receive, and Process.}
	\label{fig:idrenewaldelay}
\end{figure*}

\begin{table}[ht]
\centering
\caption{Communication Parameter Settings between Vehicle and CIP}
%\begin{center}
\begin{tabular}[t]{ c|c }
 \hline
 Parameters & Value  \\ [0.5ex]
 \hline
 Wireless Model & 802.11p\\
 Wireless Operating Mode & Adhoc Wifi Mac\\
 %Transmission Power & \\
 Propagation Loss Model & Range Propagation Loss Model\\
 WiFi Transmission Range & 100 m\\
 Propagation Delay Mode & Constant Speed Propagation Delay Model\\
 Bandwidth & 6Mbps \\
 \hline
\end{tabular}
\label{tab:vehcip}
\end{table}

In Figure~\ref{fig:idrenewaldelay}, we show the break down of the total delay for retrieving a new certificate by a vehicle. The delays showed in Figure~\ref{fig:idrenewaldelay} are averages of ten runs. The vehicle initiates the process of certificate request at $0\textsuperscript{th} ms$ and receives the certificate from manufacturer at $33\textsuperscript{rd} ms$. In our experiment scenario, the CIP supports the certificate issuance of two manufacturers, Toyota and Honda. It takes the vehicle $3.5 ms$ on average to receive the two certificates of the CIP: one signed by Honda and another by Toyota. We assume that the vehicle is manufactured by Toyota, which is one of the manufacturers supported by the CIP.  $P_1$ denotes the time it takes for the vehicle to encrypt $I_1$. After receiving $I_1$, the CIP needs to decrypt part of it using its private key. The corresponding delay is denoted by $P_2$ in the figure. The manufacturer needs to decrypt $I_2$ and generate the content of $D_1$. $P_3$ is the time required by the manufacturer to generate $D_1$.  The entire process takes $33 ms$, which means the vehicle can obtain certificates for at least 30 new pseudonyms in 1 second or at least 1800 pseudonyms in 1 minute.  This process can be further optimized by including the keys for multiple pseudonyms in one request. 
