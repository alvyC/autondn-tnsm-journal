(herald "Pseudonym Certificate Issuance through Proxy"
        (comment "Vehicles fetch a Certificate from manufacturer through a proxy"))

; QuickStart for results
; Hover over the circles to see what is being sent
; 1) Transmission: Black
; 2) Reception   : Blue (possible reception), Red (impossible reception)
; 3) Dotted line : Sent and received is different
; 4) Solid line  : Sent and received is same
; invk -> private key
;    enc a (invk c) means a signed by c
; cat used to concatenate messages

; Legend:
; a  => Vehicle's public key
; b  => CIP's public key
; c  => Manufacturer's public key
; p1 => Manufacturer's new public key associated with the new pseudonym (assume that new pseudonym is part of public key)
; m  => Manufacturer's name

(defprotocol cert basic
  (defrole vec
    ;; Declare asymmetric keys a, b, c and data m (manufacturer's name), vid, p
    (vars (a b c p1 akey) (m vid data) )
    (trace
     ;; Assuming that vehicle has b's public key and it trusts it - otherwise need to verify a lot more shapes
     (recv (cat b (enc b (invk c))) )
     ;; Encrypt manufacturer's name with b and Encrypt vid, public keys with c
     (send (enc m (enc vid a c) b) )
     ;; Receive certificate for a signed by c + new manufacturer's pseudonym cert
     (recv (enc (enc a (invk c)) (enc p1 (invk c)) a))
    )
  )

  (defrole cip
    (vars (b a c p1 akey) (m vid data))
    (trace
     (send (cat b (enc b (invk c))) )
     (recv (enc m (enc vid a c) b) )
     ;; Send (manufacturer name in plain text + other encrypted info) signed by us
     (send (cat m (enc (cat m (enc vid a c)) (invk b))) )
     ;; No need to specify this ->
     ;; for analysis can assume that manufacturer sends it directly to vehicle?
     (recv (enc (enc a (invk c)) (enc p1 (invk c)) a) )
     (send (enc (enc a (invk c)) (enc p1 (invk c)) a) )
    )
  )

  (defrole mfr
    (vars (c a b p1 akey) (m vid data))
    (trace
     ;; Receive m in plain text + encrypted vid a1, a2, c
     (recv (cat m (enc (cat m (enc vid a c)) (invk b))) )
     ;; Manufacturer verifies vid and then sends:
     ;; (Certificate for a signed by c + new manufacturer's pseudonym cert) encrypted by a
     (send (enc (enc a (invk c)) (enc p1 (invk c)) a) )
    )
    (uniq-orig p1)
  )
)

(defskeleton cert
  (vars (a b c p1 akey) (m vid data))
  (defstrand vec 3 (a a) (b b) (c c) (p1 p1))
  (non-orig (invk b))
  ;(non-orig (invk c)) ; if we include this then lots of shapes
  ; we assume that vehicle already has c
  (comment "Analyze from the vehicle's perspective"))

(defskeleton cert
  (vars (a b c p1 akey) (m data))
  (defstrand cip 5 (a a) (b b) (c c) (p1 p1))
  (non-orig (invk a) (invk b))
  (comment "Analyze from the cip's perspective"))

(defskeleton cert
  (vars (a b c p1 akey) (m vid data))
  (defstrand mfr 2 (a a) (b b) (c c) (p1 p1))
  (non-orig (invk a) (invk b) (invk p1))
  (comment "Analyze from the manufacturer's perspective"))

(defskeleton cert
  (vars (a b c p1 akey) (m vid data))
  (defstrand vec 3 (a a) (b b) (c c) (p1 p1))
  (deflistener m)
  (non-orig (invk b))
  (comment "From the vehicle's perspective, is the manufacturer name leaked?"))

(defskeleton cert
  (vars (a b c p1 akey) (m data))
  (defstrand cip 5 (a a) (b b) (c c) (p1 p1))
  (deflistener p1)
  (non-orig (invk a) (invk b) (invk p1))
  (comment "From the CIP's perspective, is the manufacturer's new certificate/name leaked?"))

(defskeleton cert
  (vars (a b c p1 akey) (m vid data))
  (defstrand mfr 2 (a a) (b b) (c c) (p1 p1))
  (deflistener p1)
  (non-orig (invk a) (invk b) (invk p1))
  (comment "From the manufacturer's perspective, is the manufacturer's new certificate/name leaked?"))
